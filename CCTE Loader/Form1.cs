﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CCTE_Loader
{
    public partial class Form1 : Form
    {
        List<DirectoryInfo> Directories;
        List<FileInfo> Files;
        List<iniStruct> ini;
        public Form1()
        {
            InitializeComponent();
            Directories = new List<DirectoryInfo>();
            Files = new List<FileInfo>();
            ini = new List<iniStruct>();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Directories.Add(new DirectoryInfo(textBox1.Text));


                while (Directories.Count > 0)
                {
                    LoadDirectory();
                }

                for (int i = 0; i < ini.Count; i++)
                {
                    ParseOriginal(ini[i]);
                }
                SaveBase();
                MessageBox.Show("Готово!");
        }

        void SaveBase()
        {
            foreach (iniStruct iS in ini)
            {
                StreamWriter sw = new StreamWriter(".\\Base" + iS.Patch, false, Encoding.GetEncoding(1251));
                sw.WriteLine("//Head");
                sw.WriteLine("Type:" + iS.Type.ToString());
                sw.WriteLine("Patch:" + iS.Patch);
                sw.WriteLine("");
                sw.WriteLine("//Data");
                foreach (iniData iD in iS.Data)
                {
                    sw.WriteLine("Name:" + iD.Name);
                    //sw.WriteLine("//Text");
                    sw.WriteLine("Orig:" + iD.OriginalText);
                    sw.WriteLine("Local:" + iD.Text);
                    sw.WriteLine("");
                }
                sw.Flush();
            }
        }

        void ParseOriginal(iniStruct data)
        {
            List<string> Line = new List<string>();
            bool found = false;
            iniData temp;
            int itt = 0;
            Line.AddRange(System.IO.File.ReadAllLines(@"" + textBox2.Text + data.Patch, Encoding.GetEncoding(1251)));

            for (int i = 0; i < data.Data.Count; i++)
            {
                foreach (string ss in Line)
                {
                    itt = ss.IndexOf("=");
                    if (-1 < itt)
                    {
                        if (data.Data[i].Name == ss.Substring(itt + 1).Trim())
                        {
                            found = true;
                        }


                        if (found)
                            if (data.Type == iniType.Menu)
                            {
                                if (-1 < ss.IndexOf("Text"))
                                {
                                    temp = data.Data[i];
                                    temp.OriginalText = ss.Substring(ss.IndexOf("=") + 1).Trim();
                                    data.Data[i] = temp;
                                    found = false;
                                    break;
                                }
                            }
                            else
                            {
                                if (-1 < ss.IndexOf("Description"))
                                {
                                    temp = data.Data[i];
                                    temp.OriginalText = ss.Substring(ss.IndexOf("=") + 1).Trim();
                                    data.Data[i] = temp;
                                    found = false;
                                    break;
                                }
                            }
                    }
                }
            }


        }

        void LoadDirectory()
        {
            DirectoryInfo[] temp = Directories[0].GetDirectories();
            Directories.RemoveAt(0);
            foreach (DirectoryInfo dir in temp)
            {
                Directories.Insert(0, dir);

                foreach (FileInfo file in dir.GetFiles("*.ini"))
                {
                    Files.Add(file);
                    ParseLocalisation(file);
                }
            }
        }

        void ParseLocalisation(FileInfo file)
        {
            List<string> Line = new List<string>();
            iniStruct inis = new iniStruct();
            inis.Name = file.Name;
            inis.Data = new List<iniData>();
            iniData inid = new iniData();
            inis.Patch = file.FullName.Substring(textBox1.Text.Length);
            //StreamWriter sw = new StreamWriter(".\\Base" + ff, false, Encoding.GetEncoding(1251));
            Line.AddRange(System.IO.File.ReadAllLines(@file.FullName, Encoding.GetEncoding(1251)));


            foreach (string ss in Line)
            {
                if (-1 < ss.IndexOf("Description"))
                {
                    inis.Type = iniType.Object;
                    break;
                }
                else if (-1 < ss.IndexOf("Text"))
                {
                    inis.Type = iniType.Menu;
                    break;
                }
            }

            foreach (string ss in Line)
            {
                if (-1 < ss.IndexOf("="))
                {
                    if (inis.Type == iniType.Menu)
                    {
                        if (-1 < ss.IndexOf("Name"))
                        {
                            inid.Name = ss.Substring(ss.IndexOf("=") + 1).Trim();
                        }
                    }
                    else
                    {
                        if (-1 < ss.IndexOf("PresetName"))
                        {
                            inid.Name = ss.Substring(ss.IndexOf("=") + 1).Trim();
                        }
                    }


                    if (inis.Type == iniType.Menu)
                    {
                        if (-1 < ss.IndexOf("Text"))
                        {
                            if (inid.Name.Length > 0)
                            {
                                inid.Text = ss.Substring(ss.IndexOf("=") + 1).Trim();
                                inis.Data.Add(inid);
                            }
                        }
                    }
                    else
                    {
                        if (-1 < ss.IndexOf("Description"))
                        {
                            if (inid.Name.Length > 0)
                            {
                                inid.Text = ss.Substring(ss.IndexOf("=") + 1).Trim();
                                inis.Data.Add(inid);
                            }
                        }
                    }
                }

            }
            ini.Add(inis);
        }
    }
}

public struct iniStruct
{
    public string Name;
    public string Patch;
    public iniType Type;
    public List<iniData> Data;

}

public struct iniData
{
    public string Name;
    public string OriginalText;
    public string Text;
    
}
public enum iniType
{
    Null,
    Menu,
    Object,
    Text
}
