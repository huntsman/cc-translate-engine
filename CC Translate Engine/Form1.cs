﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CC_Translate_Engine
{
    public partial class Form1 : Form
    {
        List<DirectoryInfo> Directories;
        List<iniStruct> ini;
        int ObjectCounts;
        public Form1()
        {
            InitializeComponent();
            Directories = new List<DirectoryInfo>();
            ini = new List<iniStruct>();
            ObjectCounts = 0;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            Directories.Add(new DirectoryInfo("./Base"));


            while (Directories.Count > 0)
            {
                LoadDirectory();
            }

            textBox2.AppendText("Файлов загружено: " + ini.Count + ".\r\n");

            for (int i = 0; i < ini.Count; i++ )
                {
                    Localize(ini[i]);
                    if (checkBox4.Checked)
                    {
                        if (!ini[i].Skip)
                            SaveFile(textBox1.Text + ini[i].Patch);
                    }
                    Line.Clear();
                }

            textBox2.AppendText("Всего объектов обработано: " + ObjectCounts + ".\r\n");
       }

        void SaveFile(string patch)
        {
            StreamWriter sw = new StreamWriter(patch, false, Encoding.GetEncoding(1251));
            foreach (string st in Line)
            {
                sw.WriteLine(st);
            }
            sw.Flush();
        }

        List<string> Line = new List<string>();
        void Localize(iniStruct data)
        {
            bool found = false;
            string tt;
            int it;
            iniStruct iS;

            try
            {
                Line.AddRange(System.IO.File.ReadAllLines(@"" + textBox1.Text + data.Patch, Encoding.GetEncoding(1251)));
            }
            catch (DirectoryNotFoundException e)
            {
                for (int i = 0; i < ini.Count; i++)
                {
                    if (ini[i].Patch == data.Patch)
                    {
                        iS = ini[i];
                        iS.Skip = true;
                        ini[i] = iS;
                        break;
                    }
                }
                if (!checkBox1.Checked)
                    textBox2.AppendText(e.Message + "\r\n");
                return;
            }
            catch (FileNotFoundException e)
            {
                for (int i = 0; i < ini.Count; i++)
                {
                    if (ini[i].Patch == data.Patch)
                    {
                        iS = ini[i];
                        iS.Skip = true;
                        ini[i] = iS;
                        break;
                    }
                }
                if (!checkBox1.Checked)
                    textBox2.AppendText(e.Message + "\r\n");
                return;
            }

            for (int i = 0; i < data.Data.Count; i++)
            {
                for (int ii = 0; ii < Line.Count; ii++)
                {
                    it = Line[ii].IndexOf("=");
                    if (-1 < it)
                    {
                        //tt = Line[ii].Substring(it + 1).Trim();
                        if (data.Data[i].Name == Line[ii].Substring(it + 1).Trim())
                        {
                            found = true;
                        }

                        if (found)
                            if (data.Type == iniType.Menu)
                            {
                                if (-1 < Line[ii].IndexOf("Text"))
                                {
                                    tt = Line[ii].Substring(Line[ii].IndexOf("=") + 1).Trim();
                                    if (data.Data[i].OriginalText == tt)
                                    {
                                        Line[ii] = Line[ii].Substring(0, (Line[ii].Length - 1) - (tt.Length - 1)) + data.Data[i].Text;
                                        ObjectCounts++;
                                    }
                                    else
                                    {
                                        if (!checkBox2.Checked)
                                            textBox2.AppendText(textBox1.Text + data.Patch + "\\" + data.Data[i].Name + ": Перевод не подходит, пропуск объекта!\r\n");
                                        if (checkBox3.Checked)
                                        {
                                            textBox2.AppendText("В базе: " + data.Data[i].OriginalText + "\r\n");
                                            textBox2.AppendText("В ориг: " + tt + "\r\n");
                                            textBox2.AppendText("\r\n");
                                        }
                                    }
                                    found = false;
                                    break;
                                }
                            }
                            else
                            {
                                if (-1 < Line[ii].IndexOf("Description"))
                                {
                                    tt = Line[ii].Substring(Line[ii].IndexOf("=") + 1).Trim();
                                    if (data.Data[i].OriginalText == tt)
                                    {
                                        Line[ii] = Line[ii].Substring(0, (Line[ii].Length - 1) - (tt.Length - 1)) + data.Data[i].Text;
                                        ObjectCounts++;
                                    }
                                    else
                                    {
                                        if (!checkBox2.Checked)
                                            textBox2.AppendText(textBox1.Text + data.Patch + "\\" + data.Data[i].Name + ": Перевод не подходит, пропуск объекта!\r\n");
                                        if (checkBox3.Checked)
                                        {
                                            textBox2.AppendText("В базе: " + data.Data[i].OriginalText + "\r\n");
                                            textBox2.AppendText("В ориг: " + tt + "\r\n");
                                            textBox2.AppendText("\r\n");
                                        }
                                    }
                                    found = false;
                                    break;
                                }
                            }
                    }
                }
            }
        }

        void LoadDirectory()
        {
            DirectoryInfo[] temp = Directories[0].GetDirectories();
            Directories.RemoveAt(0);
            foreach (DirectoryInfo dir in temp)
            {
                Directories.Insert(0, dir);

                foreach (FileInfo file in dir.GetFiles("*.ini"))
                {
                    ParseBase(file);
                }
            }
        }

        void ParseBase(FileInfo file)
        {
            List<string> Line = new List<string>();
            iniStruct inis = new iniStruct();
            inis.Name = file.Name;
            inis.Data = new List<iniData>();
            iniData inid = new iniData();
            iniData tmp = new iniData();
            bool start = true;
            bool full = false;
            Line.AddRange(System.IO.File.ReadAllLines(@file.FullName, Encoding.GetEncoding(1251)));

            foreach (string ss in Line)
            {
                if (start)
                {
                    if (-1 < ss.IndexOf("Type:"))
                    {
                        inis.Type = (iniType)Enum.Parse(typeof(iniType), Parse(ss, "Type:"));
                    }
                    if (-1 < ss.IndexOf("Patch:"))
                    {
                        inis.Patch = Parse(ss, "Patch:");
                        start = false;
                    }
 
                }


                if (-1 < ss.IndexOf("Name:"))
                {
                    inid.Name = Parse(ss, "Name:");
                }
                if (-1 < ss.IndexOf("Orig:"))
                {
                    inid.OriginalText = Parse(ss, "Orig:");
                }
                if (-1 < ss.IndexOf("Local:"))
                {
                    inid.Text = Parse(ss, "Local:");
                    full = true;
                }
                if (full)
                {
                    for (int i = 0; i < inis.Data.Count; i++)
                    {
                        if (inis.Data[i].Name == inid.Name)
                        {
                            tmp = inis.Data[i];
                            tmp.NotOne = true;
                            inis.Data[i] = tmp;
                            inid.NotOne = true;
                            break;
                        }
                    }
                    inis.Data.Add(inid);
                    full = false;
                }

            }
            ini.Add(inis);
        }

        string Parse(string Sourse, string Offset)
        {
            return Sourse.Substring(Offset.Length).Trim();
        }




    }

    public struct iniStruct
    {
        public string Name;
        public string Patch;
        public iniType Type;
        public List<iniData> Data;
        public bool Skip;

    }

    public struct iniData
    {
        public string Name;
        public string OriginalText;
        public string Text;
        public bool NotOne;

    }
    public enum iniType
    {
        Null,
        Menu,
        Object,
        Text
    }
}
